#!/bin/bash

# we cannot the this since there is no machine with QAT_4XXX hardware in Beaker as of now
# Intel promised us to provide OtherQA for qatlib, qatengine and QAT kernel patchsets
echo QATLIB nulltest is PASS
exit 0
